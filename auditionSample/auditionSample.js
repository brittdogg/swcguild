/* var start;
var end;
var step;

function checkIfNumber(input) {
    if (typeof(input) != "number") {
        // show error, start over, do not continue.
        // Bootstrap might do this, check before writing this function.
        // 
    }
}

function test(form) {
    testVar = form.start.value;
    alert("You typed: " + testVar);
}

function assignValues(form){
    // var userInput = document.getElementById("input").value;
}

function printItems(array){
    for (i = 0; i < array.length; i++){
        document.write(array[i],"<br>")
    }
}
*/

function buildArray() {
    var start = parseInt(document.getElementById("start").value);
    var end = parseInt(document.getElementById("end").value);
    var step = parseInt(document.getElementById("step").value);
    var evenNums = new Array();
    var evensList = "";

    //console.log(start, end, step);
    for (var i = start; i <= end; i += step) {
        if (i % 2 == 0) {
            evenNums.push(i);
        }
    }
    //console.log(evenNums);  
    for (var j = 0; j <= (evenNums.length - 1); j += 1) {
        var evensList = evensList + (evenNums[j] + "<br>");
    }

    document.getElementById("output1").innerHTML = "Here are the even numbers between " + start + " and " + end + " by " + step + "'s: <br>" + evensList;

}
