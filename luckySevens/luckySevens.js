//For each load of page:
var gamesPlayed = 0; /* Track # of games played since load.
Use this to only show results area after first game.
Then use this for logging. Can display to user later maybe.
*/

/* For each game:
var totalRolls = 0; // tracks number of rolls thrown in this game.
var trackAmounts = []; // array tracks total amount after each roll.
When Play is clicked, set [0] to user-input bet value.
For each roll, append total amount of money after the roll to trackAmounts.length.
From this array, derive:
- total number of rolls in the game (length minus one)
- highest amount achieved in the game (highest value in array)
- roll count at highest amount achieved (array position of highest value)
*/

/* For each roll:
 var die1 = 6; // random number 1-6
 var die2 = 6; // random number 1-6
var currentAmt = 0; // tracks amount of money user has as of last roll.
When Play is clicked, i.e. the first roll of a game, set this value to the bet.
*/

function showResults(){
    console.log("Showing results.");
    document.getElementById("results").style.display = "block";
    console.log("Changing text on Play button.");
    document.getElementById("playBtn").textContent = "Play Again";
    firstPlay = false;
}

function rollDice(){
    var die1 = 2;
    var die2 = 6;
    var currentAmt = 0;
    console.log("roll value: " + (die1 + die2));
    if (die1 + die2 == 7){
        currentAmt += 4;
        console.log("you win. your total is now " + currentAmt);
    }
    else {
        currentAmt -= 1;
        console.log("you lose. your total is now " + currentAmt);
    }
}

function playGame(){
    gamesPlayed += 1;
    console.log("Playing game #" + gamesPlayed)
    if (gamesPlayed == 1) { showResults(); }
    rollDice();
}
